package by.itstep.worksearches.utils;

import by.itstep.worksearches.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatabaseCleaner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewRepository interviewRepository;

    public void clean(){

        interviewRepository.deleteAll();
        userRepository.deleteAll();
        vacancyRepository.deleteAll();

    }
}
