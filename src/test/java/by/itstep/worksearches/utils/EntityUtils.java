package by.itstep.worksearches.utils;

import by.itstep.worksearches.entity.InterviewEntity;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.entity.VacancyEntity;

import by.itstep.worksearches.entity.enums.InterviewStatus;
import by.itstep.worksearches.entity.enums.Position;
import by.itstep.worksearches.entity.enums.UserRole;
import com.github.javafaker.Faker;

import java.sql.Date;
import java.time.LocalDate;

public class EntityUtils {


    private static Faker faker = new Faker();

    public static UserEntity prepareUser() {

        UserEntity user = new UserEntity();

        user.setRole(faker.options().option(UserRole.HR, UserRole.ADMIN, UserRole.CANDIDATE));
        user.setFirstName(faker.name().firstName());
        user.setLastName(faker.name().lastName());
        user.setPhone(faker.phoneNumber().cellPhone());
        user.setEmail(faker.internet().safeEmailAddress());
        user.setPassword(String.valueOf(faker.random().nextInt(10000000, 99999999)));
        user.setYearsOfExperience(faker.random().nextInt(0, 50));
        user.setPosition(faker.options().option(Position.QA, Position.DEVELOPER, Position.MANAGER,
                Position.ECT));

        return user;
    }

    public static VacancyEntity prepareVacancy() {

        VacancyEntity vacancy = new VacancyEntity();

        vacancy.setName(faker.letterify("??????????"));
        vacancy.setPosition(faker.options().option(Position.QA, Position.DEVELOPER, Position.MANAGER,
                Position.ECT));
        vacancy.setSalary(faker.random().nextInt(1000, 10000));
        vacancy.setCompanyName(faker.company().name());

        return vacancy;
    }

    public static InterviewEntity prepareInterview(UserEntity user, VacancyEntity vacancy) {

        InterviewEntity interview = new InterviewEntity();
        interview.setStatus(faker.options().option(InterviewStatus.FINISHED, InterviewStatus.ACCEPTED,
                InterviewStatus.REJECTED, InterviewStatus.REQUESTED));
        interview.setDate(Date.valueOf(LocalDate.now()));
        interview.setUser(user);
        interview.setVacancy(vacancy);

        return interview;
    }
}
