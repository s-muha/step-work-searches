package by.itstep.worksearches.ropository;

import by.itstep.worksearches.entity.enums.Position;
import by.itstep.worksearches.entity.VacancyEntity;
import by.itstep.worksearches.repository.VacancyRepository;
import by.itstep.worksearches.utils.DatabaseCleaner;
import by.itstep.worksearches.utils.EntityUtils;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class VacancyRepositoryTest {

    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;
    private Faker faker;

    @BeforeEach
    public void setUp() {
        faker = new Faker();
        dbCleaner.clean();
    }
    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        //given
        VacancyEntity existingVacancy = addVacancyToDb();
        Integer id = existingVacancy.getId();
        //when
        VacancyEntity foundVacancy = vacancyRepository.findById(id);
        //then
        Assertions.assertEquals(id, foundVacancy.getId());

        Assertions.assertEquals(existingVacancy.getName(), foundVacancy.getName());
        Assertions.assertEquals(existingVacancy.getPosition(), foundVacancy.getPosition());
        Assertions.assertEquals(existingVacancy.getSalary(), foundVacancy.getSalary());
        Assertions.assertEquals(existingVacancy.getCompanyName(), foundVacancy.getCompanyName());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<VacancyEntity> foundVacancies = vacancyRepository.findAll();
        // then
        Assertions.assertTrue(foundVacancies.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        // given
        VacancyEntity vacancy1 = addVacancyToDb();
        VacancyEntity vacancy2 = addVacancyToDb();
        VacancyEntity vacancy3 = addVacancyToDb();
        VacancyEntity vacancy4 = EntityUtils.prepareVacancy();
        VacancyEntity vacancy5 = EntityUtils.prepareVacancy();
        // when
        List<VacancyEntity> foundVacancies = vacancyRepository.findAll();
        // then
        Assertions.assertEquals(3, foundVacancies.size());
    }

    @Test
    public void create_happyPath() {
        // given
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        // when
        VacancyEntity createVacancy = vacancyRepository.create(vacancy);
        // then
        Assertions.assertNotNull(createVacancy.getId());
        VacancyEntity foundVacancy = vacancyRepository.findById(createVacancy.getId());

        Assertions.assertEquals(vacancy.getName(), foundVacancy.getName());
        Assertions.assertEquals(vacancy.getPosition(), foundVacancy.getPosition());
        Assertions.assertEquals(vacancy.getSalary(), foundVacancy.getSalary());
        Assertions.assertEquals(vacancy.getCompanyName(), foundVacancy.getCompanyName());

    }

    @Test
    public void update_happyPath() {
        // given
        VacancyEntity existingVacancy = addVacancyToDb();

        existingVacancy.setPosition(faker.options().option(Position.QA, Position.DEVELOPER, Position.MANAGER,
                Position.ECT));
        existingVacancy.setSalary(faker.random().nextInt(1000, 10000));
        existingVacancy.setCompanyName(faker.company().name());

        // when
        VacancyEntity updateVacancy = vacancyRepository.update(existingVacancy);

        // then
        Assertions.assertEquals(existingVacancy.getId(), updateVacancy.getId());

        VacancyEntity foundVacancy = vacancyRepository.findById(existingVacancy.getId());

        Assertions.assertEquals(existingVacancy.getName(), foundVacancy.getName());
        Assertions.assertEquals(existingVacancy.getPosition(), foundVacancy.getPosition());
        Assertions.assertEquals(existingVacancy.getSalary(), foundVacancy.getSalary());
        Assertions.assertEquals(existingVacancy.getCompanyName(), foundVacancy.getCompanyName());

    }

    @Test
    public void delete_happyPath() {
        // given
        VacancyEntity existingVacancy = addVacancyToDb();
        // when
        vacancyRepository.deleteById(existingVacancy.getId());
        // then
        VacancyEntity foundVacancy = vacancyRepository.findById(existingVacancy.getId());
        Assertions.assertNull(foundVacancy);
    }

    private VacancyEntity addVacancyToDb() {
        VacancyEntity vacancyToAdd = EntityUtils.prepareVacancy();
        return vacancyRepository.create(vacancyToAdd);
    }


}
