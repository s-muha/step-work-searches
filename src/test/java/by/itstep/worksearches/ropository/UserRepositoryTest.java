package by.itstep.worksearches.ropository;

import by.itstep.worksearches.entity.enums.Position;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.entity.enums.UserRole;
import by.itstep.worksearches.repository.UserRepository;
import by.itstep.worksearches.utils.DatabaseCleaner;
import by.itstep.worksearches.utils.EntityUtils;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;
    private Faker faker;

//    @BeforeEach
//    public void setUp(){
//        faker = new Faker();
//        dbCleaner.clean();
//    }
//    @AfterEach
//    public void shutDown(){
//        dbCleaner.clean();
//    }

    @Test
    public void findById_happyPath() {
        //given
        UserEntity existingUser = addUserToDb();
        Integer id = existingUser.getId();
        //when
        UserEntity foundUser = userRepository.findById(id);
        //then
        Assertions.assertEquals(id, foundUser.getId());

        Assertions.assertEquals(existingUser.getRole(), foundUser.getRole());
        Assertions.assertEquals(existingUser.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(existingUser.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(existingUser.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(existingUser.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(existingUser.getPosition(), foundUser.getPosition());
    }

    @Test
    public void findByEmail_happyPath() {
        //given
        UserEntity existingUser = addUserToDb();
        String email = existingUser.getEmail();
        //when
        UserEntity foundUser = userRepository.findByEmail(email);
        //then
        Assertions.assertEquals(email, foundUser.getEmail());

        Assertions.assertEquals(existingUser.getId(), foundUser.getId());
        Assertions.assertEquals(existingUser.getRole(), foundUser.getRole());
        Assertions.assertEquals(existingUser.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(existingUser.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(existingUser.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(existingUser.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(existingUser.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(existingUser.getPosition(), foundUser.getPosition());
    }

    @Test
    public void findAll_whenNoOneFound(){
        // given
        // when
        List<UserEntity> foundUsers = userRepository.findAll();
        // then
        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPath(){
        // given
        UserEntity user1 = addUserToDb();
        UserEntity user2 = addUserToDb();
        UserEntity user3 = addUserToDb();
        UserEntity user4 = EntityUtils.prepareUser();
        UserEntity user5 = EntityUtils.prepareUser();
        // when
        List<UserEntity> foundUsers = userRepository.findAll();
        // then
        Assertions.assertEquals(3, foundUsers.size());
    }

    @Test
    public void create_happyPath(){
        // given
        UserEntity user = EntityUtils.prepareUser();
        // when
        UserEntity createUser = userRepository.create(user);
        // then
        Assertions.assertNotNull(createUser.getId());
        UserEntity foundUser = userRepository.findById(createUser.getId());

        Assertions.assertEquals(user.getRole(), foundUser.getRole());
        Assertions.assertEquals(user.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(user.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(user.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(user.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(user.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(user.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(user.getPosition(), foundUser.getPosition());

    }

    @Test
    public void update_happyPath(){
        // given
        UserEntity existingUser = addUserToDb();

        existingUser.setRole(faker.options().option(UserRole.HR, UserRole.CANDIDATE, UserRole.ADMIN));
        existingUser.setPhone(faker.phoneNumber().cellPhone());
        existingUser.setEmail(faker.internet().safeEmailAddress());
        existingUser.setPassword(String.valueOf(faker.random().nextInt(10000000, 99999999)));
        existingUser.setYearsOfExperience(faker.random().nextInt(0, 50));
        existingUser.setPosition(faker.options().option(Position.QA, Position.DEVELOPER, Position.MANAGER,
                Position.ECT));
        // when
        UserEntity updateUser = userRepository.update(existingUser);
        // then
        Assertions.assertEquals(existingUser.getId(), updateUser.getId());

        UserEntity foundUser = userRepository.findById(existingUser.getId());

        Assertions.assertEquals(existingUser.getRole(), foundUser.getRole());
        Assertions.assertEquals(existingUser.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(existingUser.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(existingUser.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(existingUser.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(existingUser.getPosition(), foundUser.getPosition());
    }

    @Test
    public void delete_happyPath(){
        // given
        UserEntity existingUser = addUserToDb();
        // when
        userRepository.deleteById(existingUser.getId());
        // then
        UserEntity foundUser = userRepository.findById(existingUser.getId());
        Assertions.assertNull(foundUser);
    }

    public UserEntity addUserToDb() {
        UserEntity userToAdd = EntityUtils.prepareUser();

        return userRepository.create(userToAdd);
    }





}
