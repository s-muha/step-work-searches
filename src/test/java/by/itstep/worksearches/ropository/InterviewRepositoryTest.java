package by.itstep.worksearches.ropository;

import by.itstep.worksearches.entity.InterviewEntity;
import by.itstep.worksearches.entity.enums.InterviewStatus;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.entity.VacancyEntity;
import by.itstep.worksearches.repository.InterviewRepository;
import by.itstep.worksearches.repository.UserRepository;
import by.itstep.worksearches.repository.VacancyRepository;
import by.itstep.worksearches.utils.DatabaseCleaner;
import by.itstep.worksearches.utils.EntityUtils;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@SpringBootTest
public class InterviewRepositoryTest {

    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;
    private Faker faker;

    @BeforeEach
    public void setUp(){
        faker = new Faker();
        dbCleaner.clean();
    }
    @AfterEach
    public void shutDown(){
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        //given
        InterviewEntity existingInterview = addInterviewToDb();
        Integer id = existingInterview.getId();
        //when
        InterviewEntity foundInterview = interviewRepository.findById(id);
        //then
        Assertions.assertEquals(id, foundInterview.getId());

        Assertions.assertEquals(existingInterview.getStatus(), foundInterview.getStatus());
        Assertions.assertEquals(existingInterview.getDate(), foundInterview.getDate());
        Assertions.assertEquals(existingInterview.getUser(), foundInterview.getUser());
        Assertions.assertEquals(existingInterview.getVacancy(), foundInterview.getVacancy());
    }

    @Test
    public void findAll_whenNoOneFound(){
        // given
        // when
        List<InterviewEntity> foundInterview = interviewRepository.findAll();
        // then
        Assertions.assertTrue(foundInterview.isEmpty());
    }

    @Test
    public void findAll_happyPath(){
        // given
        InterviewEntity interview1 = addInterviewToDb();
        InterviewEntity interview2 = addInterviewToDb();
        InterviewEntity interview3 = addInterviewToDb();
        // when
        List<InterviewEntity> foundInterview = interviewRepository.findAll();
        // then
        Assertions.assertEquals(3, foundInterview.size());
    }

    @Test
    public void create_happyPath(){
        // given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        vacancyRepository.create(vacancy);
        InterviewEntity interview = EntityUtils.prepareInterview(user, vacancy);
        // when
        InterviewEntity createInterview = interviewRepository.create(interview);
        // then
        Assertions.assertNotNull(createInterview.getId());
        InterviewEntity foundInterview = interviewRepository.findById(createInterview.getId());

        Assertions.assertEquals(interview.getStatus(), foundInterview.getStatus());
        Assertions.assertEquals(interview.getDate(), foundInterview.getDate());
        Assertions.assertEquals(interview.getUser(), foundInterview.getUser());
        Assertions.assertEquals(interview.getVacancy(), foundInterview.getVacancy());
    }

    @Test
    public void update_happyPath(){
        // given
        InterviewEntity existingInterview = addInterviewToDb();

        existingInterview.setStatus(faker.options().option(InterviewStatus.REQUESTED, InterviewStatus.REJECTED,
                InterviewStatus.ACCEPTED, InterviewStatus.FINISHED));
        existingInterview.setDate(Date.valueOf(LocalDate.now()));
        // when
        InterviewEntity updateInterview = interviewRepository.update(existingInterview);
        // then
        Assertions.assertEquals(existingInterview.getId(), updateInterview.getId());

        InterviewEntity foundInterview = interviewRepository.findById(existingInterview.getId());

        Assertions.assertEquals(existingInterview.getStatus(), foundInterview.getStatus());
        Assertions.assertEquals(existingInterview.getDate(), foundInterview.getDate());
        Assertions.assertEquals(existingInterview.getUser(), foundInterview.getUser());
        Assertions.assertEquals(existingInterview.getVacancy(), foundInterview.getVacancy());

    }

    @Test
    public void delete_happyPath(){
        // given

        InterviewEntity existingInterview = addInterviewToDb();
        // when
        interviewRepository.deleteById(existingInterview.getId());
        // then
        InterviewEntity foundInterview= interviewRepository.findById(existingInterview.getId());
        Assertions.assertNull(foundInterview);
    }

    private InterviewEntity addInterviewToDb() {
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewEntity interviewToAdd = EntityUtils.prepareInterview(user, vacancy);
        return interviewRepository.create(interviewToAdd);
    }


}
