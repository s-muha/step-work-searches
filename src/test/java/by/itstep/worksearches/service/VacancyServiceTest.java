package by.itstep.worksearches.service;

import by.itstep.worksearches.dto.vacancyDto.VacancyCreateDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyFullDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyShortDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyUpdateDto;
import by.itstep.worksearches.entity.enums.Position;
import by.itstep.worksearches.entity.VacancyEntity;
import by.itstep.worksearches.mapper.VacancyMapper;
import by.itstep.worksearches.repository.VacancyRepository;
import by.itstep.worksearches.utils.DatabaseCleaner;
import by.itstep.worksearches.utils.EntityUtils;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class VacancyServiceTest {
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private VacancyService vacancyService;
    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    VacancyMapper vacancyMapper;
    private Faker faker;

    @BeforeEach
    public void setUp(){
        faker = new Faker();
        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown(){
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        //given
        VacancyEntity existingVacancy = addVacancyToDb();
        Integer id = existingVacancy.getId();

        //when
        VacancyFullDto foundVacancy = vacancyService.findById(id);
        //then
        Assertions.assertEquals(id, foundVacancy.getId());
        Assertions.assertEquals(existingVacancy.getName(), foundVacancy.getName());
        Assertions.assertEquals(existingVacancy.getPosition(), foundVacancy.getPosition());
        Assertions.assertEquals(existingVacancy.getSalary(), foundVacancy.getSalary());
        Assertions.assertEquals(existingVacancy.getCompanyName(), foundVacancy.getCompanyName());
    }

    @Test
    public void findAll_whenNoOneFound(){
        // given
        // when
        List<VacancyShortDto> foundVacancies = vacancyService.findAll();
        // then
        Assertions.assertTrue(foundVacancies.isEmpty());
    }

    @Test
    public void findAll_happyPath(){
        // given
        VacancyEntity vacancy1 = addVacancyToDb();
        VacancyEntity vacancy2 = addVacancyToDb();
        VacancyEntity vacancy3 = addVacancyToDb();

        // when
        List<VacancyShortDto> foundVacancies = vacancyService.findAll();
        // then
        Assertions.assertEquals(3, foundVacancies.size());
    }

    @Test
    public void create_happyPath(){
        // given
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        VacancyCreateDto vacancyCreateDto = vacancyMapper.mapVacancyCreateDto(vacancy);

        // when
        VacancyFullDto createVacancy = vacancyService.create(vacancyCreateDto);
        // then
        Assertions.assertNotNull(createVacancy.getId());
        VacancyEntity foundVacancy = vacancyRepository.findById(createVacancy.getId());

        Assertions.assertEquals(vacancyCreateDto.getName(), foundVacancy.getName());
        Assertions.assertEquals(vacancyCreateDto.getPosition(), foundVacancy.getPosition());
        Assertions.assertEquals(vacancyCreateDto.getSalary(), foundVacancy.getSalary());
        Assertions.assertEquals(vacancyCreateDto.getCompanyName(), foundVacancy.getCompanyName());
    }

    @Test
    public void update_happyPath(){
        // given
        VacancyEntity existingVacancy = addVacancyToDb();
        VacancyUpdateDto vacancyUpdateDto = vacancyMapper.mapVacancyUpdateDto(existingVacancy);

        vacancyUpdateDto.setName(existingVacancy.getName());
        vacancyUpdateDto.setPosition(existingVacancy.getPosition());
        vacancyUpdateDto.setSalary(existingVacancy.getSalary());
        vacancyUpdateDto.setCompanyName(existingVacancy.getCompanyName());

        vacancyUpdateDto.setName(faker.name().name());
        vacancyUpdateDto.setPosition(faker.options().option(Position.QA, Position.DEVELOPER, Position.MANAGER,
                Position.ECT));
        vacancyUpdateDto.setSalary(faker.random().nextInt(1000, 10000));
        vacancyUpdateDto.setCompanyName(faker.company().name());
        // when
        VacancyFullDto updateVacancy = vacancyService.update(vacancyUpdateDto);
        // then
        Assertions.assertEquals(existingVacancy.getId(), updateVacancy.getId());

        VacancyEntity foundVacancy = vacancyRepository.findById(existingVacancy.getId());

        Assertions.assertEquals(vacancyUpdateDto.getName(), foundVacancy.getName());
        Assertions.assertEquals(vacancyUpdateDto.getPosition(), foundVacancy.getPosition());
        Assertions.assertEquals(vacancyUpdateDto.getSalary(), foundVacancy.getSalary());
        Assertions.assertEquals(vacancyUpdateDto.getCompanyName(), foundVacancy.getCompanyName());
    }

    @Test
    public void delete_happyPath(){
        // given
        VacancyEntity existingVacancy = addVacancyToDb();
        // when
        vacancyService.deleteById(existingVacancy.getId());
        // then
        VacancyEntity foundVacancy = vacancyRepository.findById(existingVacancy.getId());
        Assertions.assertNull(foundVacancy);
    }

    private VacancyEntity addVacancyToDb() {
        VacancyEntity vacancyToAdd = EntityUtils.prepareVacancy();
        return vacancyRepository.create(vacancyToAdd);
    }
}
