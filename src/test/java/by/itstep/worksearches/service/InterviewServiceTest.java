package by.itstep.worksearches.service;

import by.itstep.worksearches.entity.InterviewEntity;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.entity.VacancyEntity;
import by.itstep.worksearches.dto.interviewDto.InterviewCreateDto;
import by.itstep.worksearches.dto.interviewDto.InterviewFullDto;
import by.itstep.worksearches.dto.interviewDto.InterviewShortDto;
import by.itstep.worksearches.dto.interviewDto.InterviewUpdateDto;
import by.itstep.worksearches.entity.enums.InterviewStatus;
import by.itstep.worksearches.mapper.InterviewMapper;
import by.itstep.worksearches.repository.InterviewRepository;
import by.itstep.worksearches.repository.UserRepository;
import by.itstep.worksearches.repository.VacancyRepository;
import by.itstep.worksearches.utils.DatabaseCleaner;
import by.itstep.worksearches.utils.EntityUtils;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@SpringBootTest
public class InterviewServiceTest {

    @Autowired
    private InterviewService interviewService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    InterviewMapper interviewMapper;
    private Faker faker;

    @BeforeEach
    public void setUp() {
        faker = new Faker();
        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        //given
        InterviewEntity existingInterview = addInterviewToDb();

        Integer id = existingInterview.getId();
        //when
        InterviewFullDto foundInterview = interviewService.findById(id);
        //then
        Assertions.assertNotNull(foundInterview);
        Assertions.assertEquals(id, foundInterview.getId());

        Assertions.assertEquals(existingInterview.getStatus(), foundInterview.getStatus());
        Assertions.assertEquals(existingInterview.getDate(), foundInterview.getDate());
        Assertions.assertEquals(existingInterview.getUser().getId(), foundInterview.getUser().getId());
        Assertions.assertEquals(existingInterview.getVacancy().getId(), foundInterview.getVacancy().getId());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<InterviewShortDto> foundInterviews = interviewService.findAll();
        // then
        Assertions.assertTrue(foundInterviews.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        // given
        InterviewEntity interview1 = addInterviewToDb();
        InterviewEntity interview2 = addInterviewToDb();
        InterviewEntity interview3 = addInterviewToDb();
        // when
        List<InterviewShortDto> foundInterviews = interviewService.findAll();
        // then
        Assertions.assertEquals(3, foundInterviews.size());
    }

    @Test
    public void create_happyPath() {
        // given
        UserEntity user = addUserToDb();
        Integer userId = user.getId();
        VacancyEntity vacancy = addVacancyToDb();
        Integer vacancyId = vacancy.getId();

        InterviewEntity interview = EntityUtils.prepareInterview(user, vacancy);
        InterviewCreateDto interviewCreateDto = interviewMapper.mapInterviewCreateDto(interview);
        // when
        InterviewFullDto createInterview = interviewService.create(interviewCreateDto);
        // then
        Assertions.assertNotNull(createInterview.getId());
        InterviewFullDto foundInterview = interviewService.findById(createInterview.getId());

        Assertions.assertEquals(InterviewStatus.REQUESTED, foundInterview.getStatus());
        Assertions.assertEquals(interviewCreateDto.getDate(), foundInterview.getDate());
        Assertions.assertEquals(interviewCreateDto.getVacancyId(), foundInterview.getVacancy().getId());
        Assertions.assertEquals(interviewCreateDto.getUserId(), foundInterview.getUser().getId());
    }

    @Test
    public void update_happyPath() {
        // given
        InterviewEntity interviewEntity = addInterviewToDb();
        InterviewUpdateDto interviewUpdateDto = interviewMapper.mapInterviewUpdateDto(interviewEntity);

        interviewUpdateDto.setStatus(getRandomStatus());
        interviewUpdateDto.setDate(Date.valueOf(LocalDate.now()));

        // when
        InterviewFullDto updateInterview = interviewService.update(interviewUpdateDto);
        // then
        Assertions.assertEquals(interviewUpdateDto.getId(), updateInterview.getId());

        InterviewFullDto foundInterview = interviewService.findById(interviewEntity.getId());

        Assertions.assertEquals(interviewUpdateDto.getStatus(), foundInterview.getStatus());
        Assertions.assertEquals(interviewUpdateDto.getDate(), foundInterview.getDate());
    }

    @Test
    public void delete_happyPath() {
        // given
        InterviewEntity existingInterview = addInterviewToDb();
        // when
        interviewService.deleteById(existingInterview.getId());
        // then
        InterviewEntity foundInterview = interviewRepository.findById(existingInterview.getId());

        Assertions.assertNull(foundInterview);
    }

    public InterviewEntity addInterviewToDb() {
        InterviewEntity interviewToAdd = EntityUtils.prepareInterview(addUserToDb(), addVacancyToDb());
        return interviewRepository.create(interviewToAdd);
    }

    public UserEntity addUserToDb() {
        UserEntity userToAdd = EntityUtils.prepareUser();
        return userRepository.create(userToAdd);
    }

    private VacancyEntity addVacancyToDb() {
        VacancyEntity vacancyToAdd = EntityUtils.prepareVacancy();
        return vacancyRepository.create(vacancyToAdd);
    }

    private InterviewStatus getRandomStatus() {
        return faker.options().option(InterviewStatus.FINISHED, InterviewStatus.ACCEPTED,
                InterviewStatus.REJECTED, InterviewStatus.REQUESTED);
    }
}
