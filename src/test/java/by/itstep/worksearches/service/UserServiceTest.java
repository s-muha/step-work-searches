package by.itstep.worksearches.service;

import by.itstep.worksearches.dto.userDto.UserCreateDto;
import by.itstep.worksearches.dto.userDto.UserFullDto;
import by.itstep.worksearches.dto.userDto.UserShortDto;
import by.itstep.worksearches.dto.userDto.UserUpdateDto;
import by.itstep.worksearches.entity.enums.Position;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.entity.enums.UserRole;
import by.itstep.worksearches.mapper.UserMapper;
import by.itstep.worksearches.repository.UserRepository;
import by.itstep.worksearches.utils.DatabaseCleaner;
import by.itstep.worksearches.utils.EntityUtils;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    private UserMapper userMapper;
    private Faker faker;

    @BeforeEach
    public void setUp() {
        faker = new Faker();
        dbCleaner.clean();
    }
    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void findById_happyPath() {
        //given
        UserEntity existingUser = addUserToDb();

        Integer id = existingUser.getId();
        //when

        UserFullDto foundUser = userService.findById(id);
        //then

        Assertions.assertNotNull(foundUser);
        Assertions.assertEquals(id, foundUser.getId());

        Assertions.assertEquals(existingUser.getRole(), foundUser.getRole());
        Assertions.assertEquals(existingUser.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(existingUser.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(existingUser.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(existingUser.getPosition(), foundUser.getPosition());
    }

    @Test
    public void findAll_whenNoOneFound() {
        // given
        // when
        List<UserShortDto> foundUsers = userService.findAll();
        // then
        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        // given
        UserEntity user1 = addUserToDb();
        UserEntity user2 = addUserToDb();
        UserEntity user3 = addUserToDb();

        // when
        List<UserShortDto> foundUsers = userService.findAll();
        // then
        Assertions.assertEquals(3, foundUsers.size());
    }

    @Test
    public void findByEmail_happyPath() {
        //given
        UserEntity existingUser = addUserToDb();
        String email = existingUser.getEmail();
        //when

        UserEntity foundUser = userService.findByEmail(email);
        //then
        Assertions.assertEquals(email, foundUser.getEmail());

        Assertions.assertEquals(existingUser.getId(), foundUser.getId());
        Assertions.assertEquals(existingUser.getRole(), foundUser.getRole());
        Assertions.assertEquals(existingUser.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(existingUser.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(existingUser.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(existingUser.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(existingUser.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(existingUser.getPosition(), foundUser.getPosition());
    }

    @Test
    public void create_happyPath() {
        // given
        UserEntity user = EntityUtils.prepareUser();
        UserCreateDto userCreateDto = userMapper.mapUserCreateDto(user);
        // when
        UserFullDto createUser = userService.create(userCreateDto);
        // then
        Assertions.assertNotNull(createUser.getId());
        UserFullDto foundUser = userService.findById(createUser.getId());

        Assertions.assertEquals(userCreateDto.getRole(), foundUser.getRole());
        Assertions.assertEquals(userCreateDto.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(userCreateDto.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(userCreateDto.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(userCreateDto.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(userCreateDto.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(userCreateDto.getPosition(), foundUser.getPosition());
    }

    @Test
    public void update_happyPath() {
        // given
        UserEntity existingUser = addUserToDb();
        UserUpdateDto userUpdateDto = userMapper.mapUserUpdateDto(existingUser);

        userUpdateDto.setRole(faker.options().option(UserRole.HR, UserRole.ADMIN, UserRole.CANDIDATE));
        userUpdateDto.setFirstName(faker.name().firstName());
        userUpdateDto.setLastName(faker.name().lastName());
        userUpdateDto.setPhone(faker.phoneNumber().cellPhone());
        userUpdateDto.setYearsOfExperience(faker.random().nextInt(0, 50));
        userUpdateDto.setPosition(faker.options().option(Position.QA, Position.DEVELOPER, Position.MANAGER,
                Position.ECT));
        // when
        UserFullDto updateUser = userService.update(userUpdateDto);
        // then
        Assertions.assertEquals(existingUser.getId(), updateUser.getId());

        UserFullDto foundUser = userService.findById(existingUser.getId());

        Assertions.assertEquals(userUpdateDto.getRole(), foundUser.getRole());
        Assertions.assertEquals(userUpdateDto.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(userUpdateDto.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(userUpdateDto.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(userUpdateDto.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(userUpdateDto.getPosition(), foundUser.getPosition());
    }

    @Test
    public void delete_happyPath() {
        // given
        UserEntity existingUser = addUserToDb();
        // when
        userService.deleteById(existingUser.getId());
        // then
        UserEntity foundUser = userRepository.findById(existingUser.getId());

        Assertions.assertNull(foundUser);
    }

    public UserEntity addUserToDb() {
        UserEntity userToAdd = EntityUtils.prepareUser();
        return userRepository.create(userToAdd);
    }

}
