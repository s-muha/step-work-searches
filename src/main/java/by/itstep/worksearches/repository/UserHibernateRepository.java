package by.itstep.worksearches.repository;

import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserHibernateRepository implements UserRepository {


    @Override
    public UserEntity findById(int id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        UserEntity foundUser = em.find(UserEntity.class, id);

        if (foundUser != null) {
            Hibernate.initialize(foundUser.getInterviews());
        }
        em.getTransaction().commit();
        em.close();

        return foundUser;
    }

    @Override
    public UserEntity findByEmail(String email) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<UserEntity> foundUsers = em
                .createNativeQuery("SELECT * FROM users WHERE users.email = '" + email + "'", UserEntity.class)
                .getResultList();
        if(foundUsers.size() == 1){
            em.getTransaction().commit();
            em.close();
            return foundUsers.get(0);
        } else {
            em.getTransaction().commit();
            em.close();
            return null;
        }
    }

    @Override
    public List<UserEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        List<UserEntity> allUsers = em.createNativeQuery("SELECT * FROM users", UserEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();
        return allUsers;
    }

    @Override
    public UserEntity create(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public UserEntity update(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        UserEntity entityToRemove = em.find(UserEntity.class, id);
        em.remove(entityToRemove);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM users").executeUpdate();

        em.getTransaction().commit();
        em.close();

    }
}
