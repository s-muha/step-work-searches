package by.itstep.worksearches.repository;

import by.itstep.worksearches.entity.InterviewEntity;

import java.util.List;

public interface InterviewRepository {

    InterviewEntity findById(int id);
    List<InterviewEntity> findAll();
    InterviewEntity create(InterviewEntity entity);
    InterviewEntity update(InterviewEntity entity);
    void deleteById(int id);

    void deleteAll();

}
