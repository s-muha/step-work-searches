package by.itstep.worksearches.repository;

import by.itstep.worksearches.entity.VacancyEntity;
import by.itstep.worksearches.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class VacancyHibernateRepository implements VacancyRepository{

    @Override
    public VacancyEntity findById(int id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        VacancyEntity foundVacancy = em.find(VacancyEntity.class, id);

        if(foundVacancy != null) {
            Hibernate.initialize(foundVacancy.getInterviews());
        }

        em.getTransaction().commit();
        em.close();

        return foundVacancy;
    }

    @Override
    public List<VacancyEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<VacancyEntity> allVacancy = em.createNativeQuery("SELECT * FROM vacancies", VacancyEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();

        return allVacancy;
    }

    @Override
    public VacancyEntity create(VacancyEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public VacancyEntity update(VacancyEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        VacancyEntity entityToRemove = em.find(VacancyEntity.class, id);
        em.remove(entityToRemove);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM vacancies").executeUpdate();

        em.getTransaction().commit();
        em.close();

    }
}
