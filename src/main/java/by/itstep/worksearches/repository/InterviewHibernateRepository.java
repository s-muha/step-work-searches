package by.itstep.worksearches.repository;

import by.itstep.worksearches.entity.InterviewEntity;
import by.itstep.worksearches.utils.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
@Repository
public class InterviewHibernateRepository implements InterviewRepository {

    @Override
    public InterviewEntity findById(int id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        InterviewEntity foundInterview = em.find(InterviewEntity.class, id);

        em.getTransaction().commit();
        em.close();

        return foundInterview;
    }

    @Override
    public List<InterviewEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<InterviewEntity> allInterview = em
                .createNativeQuery("SELECT * FROM interviews", InterviewEntity.class)
                .getResultList();

        em.getTransaction().commit();
        em.close();

        return allInterview;
    }

    @Override
    public InterviewEntity create(InterviewEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public InterviewEntity update(InterviewEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        InterviewEntity entityToRemove = em.find(InterviewEntity.class, id);
        em.remove(entityToRemove);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM interviews").executeUpdate();

        em.getTransaction().commit();
        em.close();

    }
}
