package by.itstep.worksearches.repository;

import by.itstep.worksearches.entity.UserEntity;

import java.util.List;

public interface UserRepository {

    UserEntity findById(int id);
    UserEntity findByEmail(String email);
    List<UserEntity> findAll();
    UserEntity create(UserEntity entity);
    UserEntity update(UserEntity entity);
    void deleteById(int id);
    void deleteAll();
}
