package by.itstep.worksearches.entity.enums;

public enum UserRole {
    CANDIDATE, HR, ADMIN;
}
