package by.itstep.worksearches.entity.enums;

public enum InterviewStatus {

    REQUESTED, ACCEPTED, FINISHED, REJECTED
}
