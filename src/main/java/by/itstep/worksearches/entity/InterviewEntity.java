package by.itstep.worksearches.entity;

import by.itstep.worksearches.entity.enums.InterviewStatus;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name = "interviews")
public class InterviewEntity {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private InterviewStatus status;

    @Column(name = "date")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "vacancy_id")
    private VacancyEntity vacancy;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

}
