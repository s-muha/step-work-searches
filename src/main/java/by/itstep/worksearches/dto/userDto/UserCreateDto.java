package by.itstep.worksearches.dto.userDto;

import by.itstep.worksearches.entity.enums.Position;
import by.itstep.worksearches.entity.enums.UserRole;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class UserCreateDto {

    @NotNull(message = "Role cannot be null")
    private UserRole role;

    @NotNull(message = "First name can not be null")
    @Size(min = 1, max = 20, message = "First name mast be between 1 and 20")
    private String firstName;

    @NotNull(message = "Last name can not be null")
    @Size(min = 1, max = 20, message = "Last name mast be between 1 and 20")
    private String lastName;

    @NotNull(message = "Phone can not be null")
    @Size(max = 20, message = "phone number must not exceed 20 characters")
    private String phone;

    @NotNull(message = "Email can not be null")
    @Email(message = "Email mast have valid format")
    private String email;

    @NotNull(message = "Password can not be null")
    @Size(min = 8, max = 100, message = "Password length must be between 8 and 100")
    private String password;

    @NotNull(message = "Work experience cannot be null")
    @Max(value = 99, message = "Work experience must not exceed 99 years")
    private Integer yearsOfExperience;

    @NotNull(message = "Position can not be null")
    private Position position;

}
