package by.itstep.worksearches.dto.userDto;

import by.itstep.worksearches.dto.interviewDto.InterviewShortDto;
import by.itstep.worksearches.entity.enums.Position;
import by.itstep.worksearches.entity.enums.UserRole;
import lombok.Data;

import java.util.List;

@Data
public class UserFullDto {

    private Integer id;
    private UserRole role;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private Integer yearsOfExperience;
    private Position position;
    private List<InterviewShortDto> interview;
}
