package by.itstep.worksearches.dto.vacancyDto;

import by.itstep.worksearches.entity.enums.Position;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class VacancyCreateDto {

    @NotNull(message = "Name can not be null")
    private String name;

    @NotNull(message = "Position can not be null")
    private Position position;

    private Integer salary;

    @NotNull(message = "Company name can not be null")
    private String companyName;

}
