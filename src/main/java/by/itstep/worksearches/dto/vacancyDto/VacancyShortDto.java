package by.itstep.worksearches.dto.vacancyDto;

import by.itstep.worksearches.entity.enums.Position;
import lombok.Data;

@Data
public class VacancyShortDto {

    private Integer id;
    private String name;
    private Position position;
    private Integer salary;
    private String companyName;

}
