package by.itstep.worksearches.dto.vacancyDto;

import by.itstep.worksearches.dto.interviewDto.InterviewShortDto;
import by.itstep.worksearches.entity.enums.Position;
import lombok.Data;

import java.util.List;

@Data
public class VacancyFullDto {

    private Integer id;
    private String name;
    private Position position;
    private Integer salary;
    private String companyName;
    private List<InterviewShortDto> interview;
}
