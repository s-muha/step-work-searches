package by.itstep.worksearches.dto.interviewDto;

import by.itstep.worksearches.entity.enums.InterviewStatus;
import lombok.Data;

import java.sql.Date;

@Data
public class InterviewShortDto {

    private Integer id;
    private InterviewStatus status;
    private Date date;

}
