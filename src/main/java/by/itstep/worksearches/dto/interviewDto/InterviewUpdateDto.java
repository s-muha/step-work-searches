package by.itstep.worksearches.dto.interviewDto;

import by.itstep.worksearches.entity.enums.InterviewStatus;
import lombok.Data;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class InterviewUpdateDto {

    @NotNull(message = "Id can not be null")
    private Integer id;

    @NotNull(message = "status can not be null")
    private InterviewStatus status;

    @NotNull
    @Future(message = "Date can not be past")
    private Date date;

}
