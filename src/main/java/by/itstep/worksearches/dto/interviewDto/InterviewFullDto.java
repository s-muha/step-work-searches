package by.itstep.worksearches.dto.interviewDto;

import by.itstep.worksearches.dto.userDto.UserShortDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyShortDto;
import by.itstep.worksearches.entity.enums.InterviewStatus;
import lombok.Data;

import java.sql.Date;

@Data
public class InterviewFullDto {

    private Integer id;
    private InterviewStatus status;
    private Date date;
    private VacancyShortDto vacancy;
    private UserShortDto user;
}
