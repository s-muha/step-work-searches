package by.itstep.worksearches.dto.interviewDto;

import lombok.Data;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class InterviewCreateDto {

    @NotNull
    @Future(message = "Date can not be past")
    private Date date;

    @NotNull(message = "VacancyId can not be null")
    private Integer vacancyId;

    @NotNull(message = "UserId can not be null")
    private Integer userId;
}
