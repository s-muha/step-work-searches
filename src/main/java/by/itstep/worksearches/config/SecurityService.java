package by.itstep.worksearches.config;

import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityService {

    @Autowired
    UserRepository userRepository;

    public UserEntity getAuthenticateUser(){
        String email = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
        return userRepository.findByEmail(email);
    }




}
