package by.itstep.worksearches.service;

import by.itstep.worksearches.config.SecurityService;
import by.itstep.worksearches.dto.userDto.UserCreateDto;
import by.itstep.worksearches.dto.userDto.UserFullDto;
import by.itstep.worksearches.dto.userDto.UserUpdateDto;
import by.itstep.worksearches.entity.InterviewEntity;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.dto.userDto.UserShortDto;
import by.itstep.worksearches.entity.enums.UserRole;
import by.itstep.worksearches.mapper.UserMapper;
import by.itstep.worksearches.repository.InterviewRepository;
import by.itstep.worksearches.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    SecurityService securityService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private UserMapper userMapper;

    public UserFullDto findById(int id) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        UserEntity user = userRepository.findById(id);

        if (user == null) {
            throw new RuntimeException("User not found by id: " + id);
        }
        if(!authorizedUser.getId().equals(user.getId())
                && !authorizedUser.getRole().equals(UserRole.HR)
                && !authorizedUser.getRole().equals(UserRole.ADMIN)){
            throw new RuntimeException("UserService -> You are not authorized");
        }
        System.out.println("UserService -> Found user (by id: " + id + "): " + user);
        return userMapper.map(user);
    }

    public List<UserShortDto> findAll() {
        List<UserEntity> allUsers = userRepository.findAll();
        System.out.println("UserService -> Found users: " + allUsers);
        return userMapper.map(allUsers);
    }

    public UserEntity findByEmail(String email) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        UserEntity user = userRepository.findByEmail(email);
        if (user != null) {
            throw new RuntimeException("User not found by email: " + email);
        }
        if(!authorizedUser.getId().equals(user.getId())
                && !authorizedUser.getRole().equals(UserRole.HR)
                && !authorizedUser.getRole().equals(UserRole.ADMIN)){
            throw new RuntimeException("UserService -> You are not authorized");
        }
        System.out.println("UserService -> Found user (by email: " + email + "): " + user);
        return user;
    }

    public UserFullDto create(UserCreateDto userCreateDto) {
        //TODO любой юзер может зарегится как админ или HR, и может натворить делов
        UserEntity userEntity = userMapper.map(userCreateDto);

        if (userRepository.findByEmail(userEntity.getEmail()) != null) {
            throw new RuntimeException("Email: " + userEntity.getEmail() + " is taken");
        }
        UserEntity createdUser = userRepository.create(userEntity);
        System.out.println("UserService -> Created user: " + createdUser);
        return userMapper.map(createdUser);
    }

    public UserFullDto update(UserUpdateDto userUpdateDto) {

        UserEntity authorizedUser = securityService.getAuthenticateUser();
        UserEntity userEntity = userRepository.findById(userUpdateDto.getId());

        if (userEntity == null) {
            throw new RuntimeException("UserService - > User not found by id: " + userUpdateDto.getId());
        }
        // TODO последовательность ифов
        if(!authorizedUser.getId().equals(userEntity.getId())){
            throw new RuntimeException("UserService -> You are not authorized");
        }

        userEntity.setRole(userUpdateDto.getRole());
        userEntity.setFirstName(userUpdateDto.getFirstName());
        userEntity.setLastName(userUpdateDto.getLastName());
        userEntity.setPhone(userUpdateDto.getPhone());
        userEntity.setYearsOfExperience(userUpdateDto.getYearsOfExperience());
        userEntity.setPosition(userUpdateDto.getPosition());

        UserEntity updatedUser = userRepository.update(userEntity);
        System.out.println("UserService -> Updated user: " + updatedUser);
        return userMapper.map(updatedUser);
    }

    public void deleteById(int id) {
        UserEntity authorizedUser = securityService.getAuthenticateUser(); //TODO нужна ли проверка на null
        UserEntity userToDelete = userRepository.findById(id);

        if (userToDelete == null) {
            throw new RuntimeException("User was not found by id: " + id);
        }
        if(!authorizedUser.getId().equals(userToDelete.getId()) && !authorizedUser.getRole().equals(UserRole.ADMIN)){
            throw new RuntimeException("UserService -> You are not authorized");
        }
        List<InterviewEntity> existingInterview = userToDelete.getInterviews();
        for (InterviewEntity interview : existingInterview) {
            interviewRepository.deleteById(interview.getId());
        }
        userRepository.deleteById(id);
        System.out.println("UserService -> Delete user: " + userToDelete);
    }

}
