package by.itstep.worksearches.service;

import by.itstep.worksearches.config.SecurityService;
import by.itstep.worksearches.dto.interviewDto.InterviewCreateDto;
import by.itstep.worksearches.dto.interviewDto.InterviewFullDto;
import by.itstep.worksearches.dto.interviewDto.InterviewShortDto;
import by.itstep.worksearches.dto.interviewDto.InterviewUpdateDto;
import by.itstep.worksearches.entity.InterviewEntity;
import by.itstep.worksearches.entity.enums.InterviewStatus;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.entity.VacancyEntity;
import by.itstep.worksearches.entity.enums.UserRole;
import by.itstep.worksearches.mapper.InterviewMapper;
import by.itstep.worksearches.repository.InterviewRepository;
import by.itstep.worksearches.repository.UserRepository;
import by.itstep.worksearches.repository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InterviewService {

    @Autowired
    SecurityService securityService;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewMapper interviewMapper;

    public InterviewFullDto findById(int id) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        InterviewEntity interviewEntity = interviewRepository.findById(id);
        if (interviewEntity == null) {
            throw new RuntimeException("Interview not found by id: " + id);
        }
        UserEntity foundUser = interviewEntity.getUser();
        if(!authorizedUser.getId().equals(foundUser.getId())
                && !authorizedUser.getRole().equals(UserRole.HR)
                && !authorizedUser.getRole().equals(UserRole.ADMIN)){
            throw new RuntimeException("InterviewService -> You are not authorized");
        }
        System.out.println("InterviewService -> Found interview (by id: " + id + "): " + interviewEntity);
        return interviewMapper.map(interviewEntity);
    }

    public List<InterviewShortDto> findAll() {
        UserEntity authorizedUser = securityService.getAuthenticateUser();

        if(!authorizedUser.getRole().equals(UserRole.HR) && !authorizedUser.getRole().equals(UserRole.ADMIN)){
            throw new RuntimeException("InterviewService -> You are not authorized");
        }
        List<InterviewEntity> allInterviews = interviewRepository.findAll();
        System.out.println("InterviewService -> Found users: " + allInterviews);
        return interviewMapper.map(allInterviews);
    }

    public InterviewFullDto create(InterviewCreateDto interviewCreateDto) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        UserEntity user = userRepository.findById(interviewCreateDto.getUserId());
        if (user == null) {
            throw new RuntimeException("InterviewService - > Unable to create an interview without an existing" +
                    " user in the database");
        }
        if(!authorizedUser.getId().equals(user.getId())){
            throw new RuntimeException("InterviewService -> You are not authorized");
        }

        VacancyEntity vacancy = vacancyRepository.findById(interviewCreateDto.getVacancyId());
        if (vacancy == null) {
            throw new RuntimeException("InterviewService - > Unable to create an interview without an existing" +
                    " vacancy in the database");
        }
        InterviewEntity interviewEntity = interviewMapper.map(interviewCreateDto);

        interviewEntity.setStatus(InterviewStatus.REQUESTED);
        interviewEntity.setDate(interviewCreateDto.getDate());
        interviewEntity.setUser(user);
        interviewEntity.setVacancy(vacancy);

        InterviewEntity createInterview = interviewRepository.create(interviewEntity);
        System.out.println("InterviewService -> Created interview: " + createInterview);
        return interviewMapper.map(createInterview);
    }

    public InterviewFullDto update(InterviewUpdateDto interviewUpdateDto) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        InterviewEntity interviewEntity = interviewRepository.findById(interviewUpdateDto.getId());
        if (interviewEntity == null) {
            throw new RuntimeException("InterviewService - > Can't update the interview it's not in the database");
        }
        UserEntity realUser = interviewEntity.getUser();

        if(!authorizedUser.getId().equals(realUser.getId()) && !authorizedUser.getRole().equals(UserRole.HR)){
            throw new RuntimeException("InterviewService -> You are not authorized");
        }
        interviewEntity.setStatus(interviewUpdateDto.getStatus());
        interviewEntity.setDate(interviewUpdateDto.getDate());

        InterviewEntity updatedInterview = interviewRepository.update(interviewEntity);
        System.out.println("InterviewService -> updated status and interview date: " + updatedInterview);
        return interviewMapper.map(updatedInterview);
    }

    public void deleteById(int id) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        InterviewEntity interviewToDelete = interviewRepository.findById(id);
        if (interviewToDelete == null) {
            throw new RuntimeException("Interview was not found by id: " + id);
        }
        UserEntity realUser = interviewToDelete.getUser();
        if(!authorizedUser.getId().equals(realUser.getId()) && !authorizedUser.getRole().equals(UserRole.HR)){
            throw new RuntimeException("InterviewService -> You are not authorized");
        }
        interviewRepository.deleteById(id);
        System.out.println("UserService -> Delete interview: " + interviewToDelete);
    }


}
