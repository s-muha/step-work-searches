package by.itstep.worksearches.service;

import by.itstep.worksearches.config.SecurityService;
import by.itstep.worksearches.dto.vacancyDto.VacancyCreateDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyFullDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyShortDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyUpdateDto;
import by.itstep.worksearches.entity.InterviewEntity;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.entity.VacancyEntity;
import by.itstep.worksearches.entity.enums.UserRole;
import by.itstep.worksearches.mapper.VacancyMapper;
import by.itstep.worksearches.repository.InterviewRepository;
import by.itstep.worksearches.repository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VacancyService {

    @Autowired
    SecurityService securityService;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private VacancyMapper vacancyMapper;

    public VacancyFullDto findById(int id) {
        VacancyEntity vacancy = vacancyRepository.findById(id);
        if (vacancy == null) {
            throw new RuntimeException("Vacancy not found by id: " + id);
        }
        System.out.println("VacancyService -> Found vacancy: " + vacancy);
        return vacancyMapper.map(vacancy);
    }

    public List<VacancyShortDto> findAll() {
        List<VacancyEntity> allVacancies = vacancyRepository.findAll();
        System.out.println("VacancyService -> Found vacancies: " + allVacancies);
        return vacancyMapper.map(allVacancies);
    }

    public VacancyFullDto create(VacancyCreateDto vacancyCreateDto) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        if(!authorizedUser.getRole().equals(UserRole.HR)){
            throw new RuntimeException("VacancyService -> You are not authorized");
        }
        VacancyEntity vacancyEntity = vacancyMapper.map(vacancyCreateDto);
        VacancyEntity createdVacancy = vacancyRepository.create(vacancyEntity);
        System.out.println("VacancyService -> Created vacancy: " + createdVacancy);
        return vacancyMapper.map(createdVacancy);
    }

    public VacancyFullDto update(VacancyUpdateDto vacancyUpdateDto) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        if(!authorizedUser.getRole().equals(UserRole.HR)){
            throw new RuntimeException("VacancyService -> You are not authorized");
        }
        VacancyEntity vacancyEntity = vacancyRepository.findById(vacancyUpdateDto.getId());
        if (vacancyEntity == null) {
            throw new RuntimeException("VacancyService - > Vacancy not found by id: " + vacancyUpdateDto.getId());
        }
        vacancyEntity.setName(vacancyUpdateDto.getName());
        vacancyEntity.setPosition(vacancyUpdateDto.getPosition());
        vacancyEntity.setSalary(vacancyUpdateDto.getSalary());
        vacancyEntity.setCompanyName(vacancyUpdateDto.getCompanyName());

        VacancyEntity updatedVacancy = vacancyRepository.update(vacancyEntity);
        System.out.println("VacancyService -> Updated vacancy: " + updatedVacancy);
        return vacancyMapper.map(updatedVacancy);
    }

    public void deleteById(int id) {
        UserEntity authorizedUser = securityService.getAuthenticateUser();
        if(!authorizedUser.getRole().equals(UserRole.HR)){
            throw new RuntimeException("VacancyService -> You are not authorized");
        }
        VacancyEntity vacancyToDelete = vacancyRepository.findById(id);
        if (vacancyToDelete == null) {
            throw new RuntimeException("Vacancy was not found by id: " + id);
        }
        List<InterviewEntity> existingInterview = vacancyToDelete.getInterviews();
        for (InterviewEntity interview : existingInterview) {
            interviewRepository.deleteById(interview.getId());
        }
        vacancyRepository.deleteById(id);
        System.out.println("VacancyService -> Delete vacancy: " + vacancyToDelete);
    }
}
