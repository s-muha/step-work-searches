package by.itstep.worksearches.controller;

import by.itstep.worksearches.dto.userDto.UserCreateDto;
import by.itstep.worksearches.dto.userDto.UserFullDto;
import by.itstep.worksearches.dto.userDto.UserShortDto;
import by.itstep.worksearches.dto.userDto.UserUpdateDto;
import by.itstep.worksearches.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public UserFullDto findById(@PathVariable int id) {
        UserFullDto userFullDto = userService.findById(id);
        return userFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserShortDto> findAllUsers() {
        List<UserShortDto> allUsers = userService.findAll();
        return allUsers;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserFullDto create(@Valid @RequestBody UserCreateDto userCreateDto) {
        UserFullDto userFullDto = userService.create(userCreateDto);
        return userFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public UserFullDto update(@Valid @RequestBody UserUpdateDto userUpdateDto){
        UserFullDto userFullDto = userService.update(userUpdateDto);
        return userFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        userService.deleteById(id);
    }

}
