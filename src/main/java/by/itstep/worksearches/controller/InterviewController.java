package by.itstep.worksearches.controller;

import by.itstep.worksearches.dto.interviewDto.InterviewCreateDto;
import by.itstep.worksearches.dto.interviewDto.InterviewFullDto;
import by.itstep.worksearches.dto.interviewDto.InterviewShortDto;
import by.itstep.worksearches.dto.interviewDto.InterviewUpdateDto;
import by.itstep.worksearches.service.InterviewService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class InterviewController {

    @Autowired
    InterviewService interviewService;

    @ResponseBody
    @RequestMapping(value = "/interviews/{id}", method = RequestMethod.GET)
    public InterviewFullDto findById(@PathVariable int id) {
        InterviewFullDto interviewFullDto = interviewService.findById(id);
        return interviewFullDto;
    }

    @ResponseBody
    @RequestMapping(value = "/interviews", method = RequestMethod.GET)
    public List<InterviewShortDto> findAllInterviews() {
        List<InterviewShortDto> interviewShortDtos = interviewService.findAll();
        return interviewShortDtos;
    }

    @ResponseBody
    @RequestMapping(value = "/interviews", method = RequestMethod.POST)
    public InterviewFullDto create(@Valid @RequestBody InterviewCreateDto interviewCreateDto){
        return interviewService.create(interviewCreateDto);
    }

    @ResponseBody
    @RequestMapping(value = "/interviews", method = RequestMethod.PUT)
    public InterviewFullDto update(@Valid @RequestBody InterviewUpdateDto interviewUpdateDto){
        return interviewService.update(interviewUpdateDto);
    }

    @ResponseBody
    @RequestMapping(value = "/interviews/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        interviewService.deleteById(id);
    }

}






