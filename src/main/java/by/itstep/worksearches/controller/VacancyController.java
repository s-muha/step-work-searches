package by.itstep.worksearches.controller;

import by.itstep.worksearches.dto.vacancyDto.VacancyCreateDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyFullDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyShortDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyUpdateDto;
import by.itstep.worksearches.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class VacancyController {

    @Autowired
    VacancyService vacancyService;

    @ResponseBody
    @RequestMapping(value = "/vacancies/{id}", method = RequestMethod.GET)
    public VacancyFullDto findById(@PathVariable int id){
        return vacancyService.findById(id);
    }

    @ResponseBody
    @RequestMapping(value = "/vacancies", method = RequestMethod.GET)
    public List<VacancyShortDto> findAllVacancies(){
        return vacancyService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/vacancies", method = RequestMethod.POST)
    public VacancyFullDto create(@Valid @RequestBody VacancyCreateDto vacancyCreateDto){
        return vacancyService.create(vacancyCreateDto);
    }

    @ResponseBody
    @RequestMapping(value = "/vacancies", method = RequestMethod.PUT)
    public VacancyFullDto update(@Valid @RequestBody VacancyUpdateDto vacancyUpdateDto){
        return vacancyService.update(vacancyUpdateDto);
    }

    @ResponseBody
    @RequestMapping(value = "vacancies/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        vacancyService.deleteById(id);
    }

}
