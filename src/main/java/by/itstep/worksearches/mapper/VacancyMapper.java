package by.itstep.worksearches.mapper;

import by.itstep.worksearches.dto.vacancyDto.VacancyCreateDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyFullDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyShortDto;
import by.itstep.worksearches.dto.vacancyDto.VacancyUpdateDto;
import by.itstep.worksearches.entity.VacancyEntity;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface VacancyMapper {


    VacancyFullDto map(VacancyEntity vacancyEntity);

    default List<VacancyShortDto> map(List<VacancyEntity> vacancyEntities) {
        List<VacancyShortDto> vacancyShortDtos = new ArrayList<>();

        for (VacancyEntity vacancyEntity : vacancyEntities) {
            VacancyShortDto vacancyShortDto = new VacancyShortDto();

            vacancyShortDto.setId(vacancyEntity.getId());
            vacancyShortDto.setName(vacancyEntity.getName());
            vacancyShortDto.setPosition(vacancyEntity.getPosition());
            vacancyShortDto.setSalary(vacancyEntity.getSalary());
            vacancyShortDto.setCompanyName(vacancyEntity.getCompanyName());

            vacancyShortDtos.add(vacancyShortDto);
        }
        return vacancyShortDtos;
    }

    VacancyEntity map(VacancyCreateDto vacancyCreateDto);

    VacancyShortDto mapOneVacancyInShortDto(VacancyEntity vacancyEntity);

    VacancyCreateDto mapVacancyCreateDto(VacancyEntity vacancyEntity);

    VacancyUpdateDto mapVacancyUpdateDto(VacancyEntity vacancyEntity);

}
