package by.itstep.worksearches.mapper;

import by.itstep.worksearches.dto.interviewDto.InterviewCreateDto;
import by.itstep.worksearches.dto.interviewDto.InterviewFullDto;
import by.itstep.worksearches.dto.interviewDto.InterviewShortDto;
import by.itstep.worksearches.dto.interviewDto.InterviewUpdateDto;
import by.itstep.worksearches.entity.InterviewEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface InterviewMapper {

    InterviewFullDto map(InterviewEntity interviewEntity);

    default List<InterviewShortDto> map(List<InterviewEntity> interviewEntities) {

        List<InterviewShortDto> interviewShortDtos = new ArrayList<>();

        for (InterviewEntity interviewEntity : interviewEntities) {
            InterviewShortDto interviewShortDto = new InterviewShortDto();
            interviewShortDto.setId(interviewEntity.getId());
            interviewShortDto.setStatus(interviewEntity.getStatus());
            interviewShortDto.setDate(interviewEntity.getDate());

            interviewShortDtos.add(interviewShortDto);
        }
        return interviewShortDtos;
    }

    InterviewEntity map(InterviewCreateDto interviewCreateDto);

    @Mapping(target = "vacancyId", source = "vacancy.id")
    @Mapping(target = "userId", source = "user.id")
    InterviewCreateDto mapInterviewCreateDto(InterviewEntity interviewEntity);

    InterviewUpdateDto mapInterviewUpdateDto(InterviewEntity interviewEntity);

}
