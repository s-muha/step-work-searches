package by.itstep.worksearches.mapper;

import by.itstep.worksearches.dto.userDto.UserCreateDto;
import by.itstep.worksearches.dto.userDto.UserShortDto;
import by.itstep.worksearches.dto.userDto.UserUpdateDto;
import by.itstep.worksearches.entity.UserEntity;
import by.itstep.worksearches.dto.userDto.UserFullDto;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserFullDto map(UserEntity userEntity);

    default List<UserShortDto> map(List<UserEntity> userEntities){
        List<UserShortDto> userShortDtos = new ArrayList<>();
        for(UserEntity userEntity: userEntities){
            UserShortDto userShortDto = new UserShortDto();

            userShortDto.setId(userEntity.getId());
            userShortDto.setRole(userEntity.getRole());
            userShortDto.setFirstName(userEntity.getFirstName());
            userShortDto.setLastName(userEntity.getLastName());
            userShortDto.setPhone(userEntity.getPhone());
            userShortDto.setEmail(userEntity.getEmail());
            userShortDto.setYearsOfExperience(userEntity.getYearsOfExperience());
            userShortDto.setPosition(userEntity.getPosition());

            userShortDtos.add(userShortDto);
        }
        return userShortDtos;
    }

    UserEntity map(UserCreateDto userCreateDto);

    UserShortDto mapOneUserInShortDto(UserEntity userEntity);

    UserCreateDto mapUserCreateDto(UserEntity userEntity);

    UserUpdateDto mapUserUpdateDto(UserEntity userEntity);



}
