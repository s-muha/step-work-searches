package by.itstep.worksearches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkSearchesApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkSearchesApplication.class, args);

	}

}
